// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package models

const (
	ErrorBinding    = 98
	ErrorService    = 99
	ErrorUploadFile = 95
	ErrorOpenFile   = 96
	ErrorCopyFile   = 97
)

type PetRegisterRequest struct {
	ID    int    `json:"id" binding:"exists"`
	Name  string `json:"name" binding:"exists"`
	Age   int    `json:"age" binding:"exists"`
}

type PetUpdateRequest struct {
	Name  string `json:"name" binding:"exists"`
	Age   int    `json:"age" binding:"exists"`
}

type Pet struct {
	ID    int    `db:"id" json:"id" binding:"exists"`
	Name  string `db:"name" json:"name" binding:"exists"`
	Age   int    `db:"age" json:"age" binding:"exists"`
	Photo string `db:"photo" json:"photo" binding:"exists"`
}

type PetResponse struct {
	Data    Pet `json:"data"`
	Status  string  `json:"status"`
	Message string  `json:"message"`
}
