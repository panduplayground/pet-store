// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package models


// Request : Model for Content
type Request struct {
	Header      interface{} `json:"header"`
	URL         interface{} `json:"url"`
	RequestBody interface{} `json:"body"`
}

// Response : Model for Response
type Responses struct {
	Header       interface{} `json:"header"`
	URL          interface{} `json:"url"`
	ResponseBody interface{} `json:"body"`
}

// LogModel : Model for Log
type LogModel struct {
	Tag      string      `json:"tag"`
	TagName  string      `json:"tagName"`
	Metadata string      `json:"metadata"`
	LogTime  string      `json:"logTime"`
	Message  string      `json:"message"`
	Headers  string      `json:"headers"`
	Content  interface{} `json:"content"`
}

// LogStart : Model for LogStart
type LogStart struct {
	URL        string      `json:"url"`
	SubContent interface{} `json:"subContent"`
}

// LogEnd : Model for LogEnd
type LogEnd struct {
	URL          string      `json:"url"`
	ResponseTime string      `json:"responseTime"`
	SubContent   interface{} `json:"subContent"`
}

// LogHitEndPoint : Model for LogHitEndPoint
type LogHitEndPoint struct {
	URL       string      `json:"url"`
	Body      interface{} `json:"body"`
	Responses interface{} `json:"responses"`
}

// LogSubProcessEnd : Model for LogSubProcessEnd
type LogSubProcessEnd struct {
	ProsesTime string      `json:"prosesTime"`
	SubContent interface{} `json:"subContent"`
}