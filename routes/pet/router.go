// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package pet

import (
	"github.com/labstack/echo"

	"bitbucket.com/pet-store/src/handlers"
)
func Load(e *echo.Echo) *echo.Echo {
	e.POST("/pet", handlers.RegisterPet)
	e.GET("/pet/:id", handlers.FindPet)
	e.PUT("/pet/:id", handlers.UpdatePet)
	e.POST("/pet/:id/UploadImage", handlers.UploadImage)
	e.DELETE("/pet/:id", handlers.DeletePet)

	return e
}