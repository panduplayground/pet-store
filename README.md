Pet - Store App
============================


### 1. Chosen tech stack.
Go Programming Language

### 2. A brief explanation of why a particular stack was chosen.
I’ve heard a lot about Go from one co-worker and wanted to give it a try. I find it quite a low level language and miss a lot of the generics, type inheritance, 
classes from the .NET world (but over time I’ll adapt). Nonetheless I can’t help but be amazed by a few things: the code is easy to read, 
the build & tests run in no time, the code compiles to binaries (no more virtual machine!), and dependencies can be packed in plain text into a vendor directory.

Go or Golang is a language created by Google. It’s a native programming language which produces binary programs that run directly on top of machine hardware. 
It has cross platforms so the source code for windows is the same as Linux. The deployment is easy (just copy paste the binary) and the performance is highly scaleable.
Coding in Golang is really simple and during my early days it felt like writing a scripting language.
      
### 3. Infrastructure requirements for running your solution.
- Windows 10 Pro 64 - Bit
- Golang must be installed at Operating System that you used
- MySQL must be installed at Operationg System that you used
- Github must be installed at Operationg System that you used
    
### 4. Directory structure of your project.
* conf 
    + dev.yaml 
* docs
    + db
        + pet_store_db.sql
    + test
        + postman
            + link.txt
* log 
* models
    + log.go
    + pet.go
* src
    + config
        + config.go
    + database
        + database.go
    + handlers
        + handlers.go
        + ping.go
    + helpers
        + common.go
        + handlerLogger.go
    + logging
        + logging.go
    + repositories
        + db.go
        + repository.go
    + services
        + service.go
    + templates
        + json.go
* routes
    + pet
        + router.go
    + ping.go
* upload
* vendor
* .gitignore
* Gopkg.lock
* Gopkg.toml
* main.go
* README.md

### 5. Setup Instructions.
* Executable on Windows :
    - Open command prompt or cmd.
    - Make change directory or cd to pet-store app folder.
    - Type in command prompt : go build.
    - In pet-store app folder will show file pet-store.exe.
    - Then click on pet-store.exe, you pet-store app will run. 
* Binary on Windows :
    - Open command prompt or cmd.
    - Make change directory or cd to pet-store app folder.
    - Type in command prompt : set GOOS=linux.
    - Type in command prompt : set GOARCH=amd64.
    - Type in command prompt : go build main.go.
    - In pet-store app folder will show file main that not have extension.
    - Binary file only can be run at UNIX Environment. 