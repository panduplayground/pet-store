// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package main

import (
	"encoding/hex"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	uuid "github.com/satori/go.uuid"
	"github.com/tylerb/graceful"

	"bitbucket.com/pet-store/routes"
	"bitbucket.com/pet-store/routes/pet"
	"bitbucket.com/pet-store/src/config"
	"bitbucket.com/pet-store/src/database"
	"bitbucket.com/pet-store/src/helpers"
	"bitbucket.com/pet-store/src/logging"
)

func main() {
	config.InitConfig()
	logPath := config.GetString("log.path")
	port := config.GetString("port")
	logging.InitLogger(logPath)

	u2 := uuid.NewV4()
	metadata := hex.EncodeToString(u2.Bytes()[:4])
	_, err := database.DB()
	if err != nil {
		logging.LogTS(metadata, "LogTS", "Log Start", "Error DB ", "Terdapat kesalahan sistem, mohon coba beberapa saat lagi")
	}
	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))

	routes.Ping(e)
	pet.Load(e)

	logging.LogTS(metadata, "LogTS", "Log Start", "Pet-Store App now serve on port "+port, "PID: "+strconv.Itoa(os.Getpid()))
	graceful.Run(port, 10*time.Second, helpers.RequestLogger{Handle: e, Logger: logging.Log})
}