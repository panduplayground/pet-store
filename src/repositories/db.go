// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package repositories

import (
	"sync"

	"github.com/jmoiron/sqlx"

	"bitbucket.com/pet-store/models"
	"bitbucket.com/pet-store/src/database"
)

type DB struct {
	orm   *sqlx.DB
}

var repoInstance Repository
var onceRepo sync.Once

func NewRepository() Repository {
	db, err := database.DB()
	if err != nil {
	}
	onceRepo.Do(func() {
		repoInstance = &DB{
			orm: db,
		}
	})
	return repoInstance
}

func (db *DB) Save(pet models.Pet) (models.Pet, error) {
	tx := db.orm.MustBegin()

	_, errExec := tx.Exec(`INSERT INTO tbl_pets (id, name, age, photo) VALUES (?, ?, ?, ?)`, pet.ID, pet.Name, pet.Age, pet.Photo)
	if errExec != nil {
		return pet, errExec
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		return pet, errCommit
	}

	return pet, nil
}

func (db *DB) FindById(id int) (models.Pet, error) {
	pet := models.Pet{}

	tx := db.orm.MustBegin()

	stmt, errPrepare := tx.Preparex(`SELECT * FROM tbl_pets WHERE id=?`)
	if errPrepare != nil {
		return pet, errPrepare
	}

	errGet := stmt.Get(&pet, id)
	if errGet != nil {
		return pet, errGet
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		return pet, errCommit
	}

	return pet, nil
}

func (db *DB) UpdateById(pet models.Pet) (models.Pet, error) {
	tx := db.orm.MustBegin()

	_, errExec := tx.Exec(`UPDATE tbl_pets set name=?, age=? where id=?`, pet.Name, pet.Age, pet.ID)
	if errExec != nil {
		return pet, errExec
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		return pet, errCommit
	}

	return pet, nil
}

func (db *DB) DeleteById(id int) error {
	tx := db.orm.MustBegin()

	_, errExec := tx.Exec(`DELETE from tbl_pets where id=?`, id)
	if errExec != nil {
		return errExec
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		return errCommit
	}

	return nil
}

func (db *DB) UpdateImageById(pet models.Pet) (models.Pet, error) {
	tx := db.orm.MustBegin()

	_, errExec := tx.Exec(`UPDATE tbl_pets set photo=? where id=?`, pet.Photo, pet.ID)
	if errExec != nil {
		return pet, errExec
	}

	errCommit := tx.Commit()
	if errCommit != nil {
		return pet, errCommit
	}

	return pet, nil
}


