// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package repositories

import (
	"bitbucket.com/pet-store/models"
)

type Repository interface {
	Save(pet models.Pet) (models.Pet, error)
	FindById(id int) (models.Pet, error)
	UpdateById(pet models.Pet) (models.Pet, error)
	DeleteById(id int) error
	UpdateImageById(pet models.Pet) (models.Pet, error)
}