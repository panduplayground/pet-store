// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package services

import (
	"sync"

	"bitbucket.com/pet-store/models"
	"bitbucket.com/pet-store/src/repositories"
)

type Service interface {
	RegisterPet(petReq *models.PetRegisterRequest) (models.Pet, error)
	FindPet(id int) (models.Pet, error)
	UpdatePet(petReq *models.PetUpdateRequest, id int) (models.Pet, error)
	DeletePet(id int) error
	UpdatePhotoPet(fileName string, id int) (models.Pet, error)
}

type ServiceImpl struct {
	Repo repositories.Repository
}

var instance Service
var once sync.Once

func NewService() Service {
	once.Do(func() {
		instance = &ServiceImpl{
			Repo: repositories.NewRepository(),
		}
	})

	return instance
}

func (s *ServiceImpl) RegisterPet(petReq *models.PetRegisterRequest) (models.Pet, error) {
	petAdded := models.Pet{
		ID : petReq.ID,
		Name: petReq.Name,
		Age: petReq.Age,
		Photo: "",
	}

	result, err := s.Repo.Save(petAdded)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *ServiceImpl) FindPet(id int) (models.Pet, error) {
	result, err := s.Repo.FindById(id)
	if err != nil {
		return result, err
	}

	return result, nil
}

func (s *ServiceImpl) UpdatePet(petReq *models.PetUpdateRequest, id int) (models.Pet, error) {
	petUpdated := models.Pet{
		ID: id,
		Name: petReq.Name,
		Age: petReq.Age,
	}

	result, errCheckExists := s.Repo.FindById(id)
	if errCheckExists != nil {
		return result, errCheckExists
	} else {
		result, errUpdate := s.Repo.UpdateById(petUpdated)
		if errUpdate != nil {
			return result, errUpdate
		} else {
			result, errAfterUpdate := s.Repo.FindById(id)
			if errAfterUpdate != nil {
				return result, errAfterUpdate
			}

			return result, nil
		}
	}
}

func (s *ServiceImpl) DeletePet(id int) error {
	_, errCheckExists := s.Repo.FindById(id)
	if errCheckExists != nil {
		return errCheckExists
	} else {
		errDelete := s.Repo.DeleteById(id)
		if errDelete != nil {
			return errDelete
		}

		return nil
	}
}

func (s *ServiceImpl) UpdatePhotoPet(fileName string, id int) (models.Pet, error) {
	petUpdated := models.Pet{
		ID: id,
		Photo: fileName,
	}

	result, errCheckExists := s.Repo.FindById(id)
	if errCheckExists != nil {
		return result, errCheckExists
	} else {
		result, err := s.Repo.UpdateImageById(petUpdated)
		if err != nil {
			return result, err
		} else {
			result, errAfterUpdatePhoto := s.Repo.FindById(id)
			if errAfterUpdatePhoto != nil {
				return result, errAfterUpdatePhoto
			}

			return result, nil
		}
	}
}
