// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package handlers

import (
	"encoding/json"
	"io"
	"os"
	"strconv"

	"github.com/labstack/echo"
	uuid "github.com/satori/go.uuid"

	"bitbucket.com/pet-store/models"
	"bitbucket.com/pet-store/src/config"
	"bitbucket.com/pet-store/src/logging"
	"bitbucket.com/pet-store/src/services"
	"bitbucket.com/pet-store/src/templates"
)

func RegisterPet(c echo.Context) error {
	resp := models.PetResponse{}
	data := models.Pet{}
	petRegisterReq := new(models.PetRegisterRequest)
	if err := c.Bind(petRegisterReq); err != nil {
		return templates.RenderErrorResponse(c, models.ErrorBinding, err.Error())
	}

	jsonStr, errMa := json.Marshal(petRegisterReq)
	if errMa != nil {
		return templates.RenderErrorResponse(c, models.ErrorBinding, errMa.Error())
	}

	uid := uuid.NewV4()
	logging.LogTS(uid.String(), c.Path(), string(jsonStr), c.Path(), string(jsonStr))

	registerSvc := services.NewService()
	data, errSvc := registerSvc.RegisterPet(petRegisterReq)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	resp.Data = data
	resp.Status = "00"
	resp.Message = "Register Pet Successfully"
	return templates.ResponseGeneral(c, resp)
}

func FindPet(c echo.Context) error {
	resp := models.PetResponse{}
	data := models.Pet{}
	idPet, _ := strconv.Atoi(c.Param("id"))

	uid := uuid.NewV4()
	logging.LogTS(uid.String(), c.Path(), strconv.Itoa(idPet), c.Path(), strconv.Itoa(idPet))

	findSvc := services.NewService()
	data, errSvc := findSvc.FindPet(idPet)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	resp.Data = data
	resp.Status = "00"
	resp.Message = "Find Pet Successfully"
	return templates.ResponseGeneral(c, resp)
}

func UpdatePet(c echo.Context) error {
	resp := models.PetResponse{}
	data := models.Pet{}
	petUpdateReq := new(models.PetUpdateRequest)
	idPet, _ := strconv.Atoi(c.Param("id"))
	if err := c.Bind(petUpdateReq); err != nil {
		return templates.RenderErrorResponse(c, models.ErrorBinding, err.Error())
	}

	jsonStr, errMa := json.Marshal(petUpdateReq)
	if errMa != nil {
		return templates.RenderErrorResponse(c, models.ErrorBinding, errMa.Error())
	}

	uid := uuid.NewV4()
	logging.LogTS(uid.String(), c.Path(), string(jsonStr), c.Path(), string(jsonStr))

	updateSvc := services.NewService()
	data, errSvc := updateSvc.UpdatePet(petUpdateReq, idPet)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	resp.Data = data
	resp.Status = "00"
	resp.Message = "Update Pet Successfully"
	return templates.ResponseGeneral(c, resp)
}

func DeletePet(c echo.Context) error {
	resp := models.PetResponse{}
	data := models.Pet{}
	idPet, _ := strconv.Atoi(c.Param("id"))

	uid := uuid.NewV4()
	logging.LogTS(uid.String(), c.Path(), strconv.Itoa(idPet), c.Path(), strconv.Itoa(idPet))

	deleteSvc := services.NewService()

	data, errSvc := deleteSvc.FindPet(idPet)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	errSvc = deleteSvc.DeletePet(idPet)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	resp.Data = data
	resp.Status = "00"
	resp.Message = "Delete Pet Successfully"
	return templates.ResponseGeneral(c, resp)
}

func UploadImage(c echo.Context) error {
	var uploadPath = config.GetString("uploadPath")
	resp := models.PetResponse{}
	data := models.Pet{}
	idPet, _ := strconv.Atoi(c.Param("id"))
	file, errUpload := c.FormFile("image")
	if errUpload != nil {
		return templates.RenderErrorResponse(c, models.ErrorUploadFile, errUpload.Error())
	}

	src, errOpen := file.Open()
	if errOpen != nil {
		return templates.RenderErrorResponse(c, models.ErrorOpenFile, errOpen.Error())
	}
	defer src.Close()

	f, errOpenFile := os.OpenFile(uploadPath + file.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if errOpenFile != nil {
		return templates.RenderErrorResponse(c, models.ErrorOpenFile, errOpenFile.Error())
	}
	defer f.Close()

	_, errCopy := io.Copy(f, src)
	if errCopy != nil {
		return templates.RenderErrorResponse(c, models.ErrorCopyFile, errCopy.Error())
	}

	uid := uuid.NewV4()
	logging.LogTS(uid.String(), c.Path(), strconv.Itoa(idPet) + " - " + file.Filename, c.Path(), strconv.Itoa(idPet) + " - " + file.Filename)

	updatePhotoSvc := services.NewService()
	data, errSvc := updatePhotoSvc.UpdatePhotoPet(file.Filename, idPet)
	if errSvc != nil {
		return templates.RenderErrorResponse(c, models.ErrorService, errSvc.Error())
	}

	resp.Data = data
	resp.Status = "00"
	resp.Message = "Upload Image Pet Successfully"
	return templates.ResponseGeneral(c, resp)
}


