// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package templates

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo"
	uuid "github.com/satori/go.uuid"

	"bitbucket.com/pet-store/src/config"
	"bitbucket.com/pet-store/src/helpers"
)

//Response is main return response
type Response struct {
	RequestID    string      `json:"request_id"`
	Code         int         `json:"code"`
	Data         interface{} `json:"data"`
	MessageError string      `json:"error_message"`
	ProcessTime  float64     `json:"server_process_time"`
	App          string      `json:"app"`
}

type GeneralResponse struct {
	Message    string      `json:"message"`
	Status         string       `json:"status"`
	Data         interface{} `json:"data"`

}

//ErrorResponse define response for error return
type ErrorResponse struct {
	RequestID    string      `json:"request_id"`
	Code         int         `json:"status"`
	Data         interface{} `json:"data"`
	MessageError string      `json:"error_message"`
	ProcessTime  float64     `json:"server_process_time"`
	App          string      `json:"app"`
}

//ResponseLink handle response for link
type ResponseLink struct {
	Self    string      `json:"self,omitempty"`
	Related interface{} `json:"related,omitempty"`
}

//RenderResponse used to render general response with mapping data
func RenderResponse(c echo.Context, data interface{}) error {
	c.Response().Header().Set("Access-Control-Allow-Credentials", "true")
	c.Response().Header().Set("Access-Control-Allow-Origin", c.Request().Header.Get("Origin"))
	contentType := c.Request().Header.Get(echo.HeaderContentType)
	if strings.Contains(c.Request().Header.Get(echo.HeaderContentType), "multipart/form-data") {
		contentType = "application/vnd.api+json"
	} else if strings.Compare(contentType, "") == 0 {
		contentType = "application/json"
	}
	c.Response().Header().Set(echo.HeaderContentType, contentType)
	c.Response().WriteHeader(http.StatusOK)

	if data == nil {
		data = []string{}
	}

	response := &GeneralResponse{
		Status:  "00",
		Message: "Success",
		Data:         data,
	}

	return json.NewEncoder(c.Response()).Encode(response)
}

//ResponseGeneral used to render general response with data
func ResponseGeneral(c echo.Context, data interface{}) error {
	c.Response().Header().Set("Access-Control-Allow-Credentials", "true")
	c.Response().Header().Set("Access-Control-Allow-Origin", c.Request().Header.Get("Origin"))
	contentType := c.Request().Header.Get(echo.HeaderContentType)
	if strings.Contains(c.Request().Header.Get(echo.HeaderContentType), "multipart/form-data") {
		contentType = "application/vnd.api+json"
	} else if strings.Compare(contentType, "") == 0 {
		contentType = "application/json"
	}
	c.Response().Header().Set(echo.HeaderContentType, contentType)
	c.Response().WriteHeader(http.StatusOK)

	if data == nil {
		data = []string{}
	}

	return json.NewEncoder(c.Response()).Encode(data)
}

// RenderErrorResponse  Render error messages
// statusCode: 400 (Bad Request, user input validation)/ 403 (Forbidden, unauthorized user access)/ 500 (Internal server error)
// errs: Error Messages
func RenderErrorResponse(c echo.Context, statusCode int, errs ...string) error {

	c.Response().Header().Set("Access-Control-Allow-Credentials", "true")
	c.Response().Header().Set("Access-Control-Allow-Origin", c.Request().Header.Get("Origin"))
	c.Response().Header().Set(echo.HeaderContentType, c.Request().Header.Get(echo.HeaderContentType))

	processTime := time.Now().Sub(config.GetTime("time_start")).Seconds() / 10000
	id := fmt.Sprintf("%s.%s.%s", c.Request().Header.Get("client_id"), c.Request().Header.Get("client_version"), c.Request().Header.Get("session_id"))

	responseError := &ErrorResponse{
		RequestID:    helpers.RandomString(20, id),
		Code:         statusCode,
		MessageError: strings.Join(errs, ", "),
		Data:         []string{},
		ProcessTime:  processTime,
		App:          "pet-store-app",
	}

	response := &GeneralResponse{
		Status:  strconv.Itoa(statusCode),
		Message: responseError.MessageError,
		Data:    responseError,
	}

	return json.NewEncoder(c.Response()).Encode(response)
}

// RenderRecover Render status if error happend and recovered
func RenderRecover(c echo.Context) error {
	uuid := uuid.NewV4()
	m := fmt.Sprintf(
		`{"request_id":"%s","code":%d,"error_message":["%s"],"data":[]}`,
		uuid,
		http.StatusRequestTimeout,
		"Terdapat kesalahan sistem, mohon coba beberapa saat lagi")
	return c.JSONBlob(http.StatusRequestTimeout, []byte(m))
}