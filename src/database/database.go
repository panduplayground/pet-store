// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package database

import (
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"strconv"
	"sync"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	uuid "github.com/satori/go.uuid"

	"bitbucket.com/pet-store/src/config"
	"bitbucket.com/pet-store/src/logging"
)

var database *sqlx.DB
var once sync.Once

func DB() (*sqlx.DB, error) {
	var err error
	once.Do(func() {
		dbHost := config.GetString(`db.host`)
		dbPort := config.GetString(`db.port`)
		dbUser := config.GetString(`db.user`)
		dbPass := config.GetString(`db.pass`)
		dbName := config.GetString(`db.name`)
		maxIdle := config.GetInt(`db.maxIdle`)
		maxOpenConn := config.GetInt(`db.maxOpenConn`)

		u2 := uuid.NewV4()
		metadata := hex.EncodeToString(u2.Bytes()[:4])

		connectionString := fmt.Sprintf("%s:%s@(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)
		database, err = sqlx.Connect("mysql", connectionString)

		if err != nil {
			log.Println("Gagal terhubung ke Database : ", err)
		}

		_ = database.Ping()
		database.SetMaxIdleConns(maxIdle)
		database.SetMaxOpenConns(maxOpenConn)

		logging.LogTS(metadata, "LogTS", "Log Start", "Database Pet-Store App is running", "PID: "+strconv.Itoa(os.Getpid()))
	})

	return database, err
}






