// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package logging

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"gopkg.in/natefinch/lumberjack.v2"

	"bitbucket.com/pet-store/models"
	"bitbucket.com/pet-store/src/config"
)

var Log *log.Logger

func InitLogger(logPath string) {
	flag.Parse()

	var writeOnLog = config.GetBool("log.write")
	var maxSize= config.GetInt("log.size")
	var maxBackups = config.GetInt("log.backups")
	var maxAge = config.GetInt("log.age")
	var compress = config.GetBool("log.compress")
	var localTime = config.GetBool("log.localTime")

	t := time.Now().Local()
	s := t.Format("2006-01-02")
	name, err := os.Hostname()

	var fileName = logPath + "/PET_STORE"+name+"_"+s+".log"

	if err != nil {
		panic(err)
	}

	var file, err1 = os.OpenFile(fileName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)

	if err1 != nil {
		panic(err1)
	}

	if writeOnLog {
		Log = log.New(file, "", log.Lmicroseconds)
		Log.SetOutput(&lumberjack.Logger{
			Filename:   fileName,
			MaxSize:    maxSize, // megabytes
			MaxBackups: maxBackups,   // backups
			MaxAge:     maxAge,  // days
			Compress:   compress,
			LocalTime:  localTime,
		})
	}
}

func LogTS(metadata string, tagName string, message string, url string, content interface{}) time.Time {
	now := time.Now()

	logStart := models.LogStart{
		URL:        url,
		SubContent: content,
	}

	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}

	// logTime := fmt.Sprintf("%s", now.In(loc).Format(time.RFC3339))
	logTime := fmt.Sprintf("%s", now.In(loc).Format("2006-01-02 15:04:05.999"))
	messageLog := models.LogModel{
		Tag:      "TS",
		TagName:  tagName,
		Metadata: metadata,
		LogTime:  logTime,
		Message:  message,
		Content:  logStart,
	}
	Log.Printf(logTime+" "+objectToJSON(messageLog))
	return now
}

// LogTG : func fot LogTG
func LogTG(metadata string, tagName string, message string, content interface{}) time.Time {
	now := time.Now()

	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}

	// logTime := fmt.Sprintf("%s", now.In(loc).Format(time.RFC3339))
	logTime := fmt.Sprintf("%s", now.In(loc).Format("2006-01-02 15:04:05.999"))
	messageLog := models.LogModel{
		Tag:      "TG",
		TagName:  tagName,
		Metadata: metadata,
		LogTime:  logTime,
		Message:  message,
		Content:  content,
	}
	Log.Printf(logTime+" "+objectToJSON(messageLog))
	return now
}

func LogTGHitEndPoint(metadata string, tagName string, message string, url string, method string, body interface{}, resp interface{}) time.Time {
	now := time.Now()

	logHit := models.LogHitEndPoint{
		URL:       url,
		Body:      body,
		Responses: resp,
	}

	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}

	logTime := fmt.Sprintf("%s", now.In(loc).Format(time.RFC3339))

	messageLog := models.LogModel{
		Tag:      "TGH",
		TagName:  tagName,
		Metadata: metadata,
		LogTime:  logTime,
		Message:  message,
		Content:  logHit,
	}
	Log.Printf(objectToJSON(messageLog))

	return now
}

// LogTE : func fot LogTE
func LogTE(metadata string, tagName string, message string, url string, content interface{}, timeT1 time.Time) {
	now := time.Now()
	timeProcess := now.Sub(timeT1)

	logEnd := models.LogEnd{
		URL:          url,
		ResponseTime: fmt.Sprintf("%d ms", timeProcess.Nanoseconds()/1000),
		SubContent:   content,
	}

	loc, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		panic(err)
	}

	logTime := fmt.Sprintf("%s", now.In(loc).Format(time.RFC3339))

	messageLog := models.LogModel{
		Tag:      "TE",
		TagName:  tagName,
		Metadata: metadata,
		LogTime:  logTime,
		Message:  message,
		Content:  logEnd,
	}
	Log.Printf(objectToJSON(messageLog))
}

// objectToJson : func for mapping object to JSON
func objectToJSON(obj interface{}) string {
	res, _ := json.Marshal(obj)
	return string(res)
}
