// Copyright 2019 Pandu Satria Nur Ananda (panduplayground)
//
// Licensed under the Apache License, Version 2.0 (the "License"): you may
// not use this file except in compliance with the License. You may obtain
// a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.

package config

import (
	"log"
	"time"

	"github.com/spf13/viper"
)

var v *viper.Viper

func init() {
	v = viper.New()
}

// InitConfig init config
func InitConfig() {
	v.Set("time_start", time.Now())
	v.SetConfigType("yaml")
	v.SetConfigName("dev")
	v.AddConfigPath("./conf")
	if err := v.ReadInConfig(); err != nil {
		log.Println(err)
	}
}

// GetString get string value from config
func GetString(key string) string { return v.GetString(key) }

// GetBool get bool value from config
func GetBool(key string) bool { return v.GetBool(key) }

// GetInt get int value from config
func GetInt(key string) int { return v.GetInt(key) }

// GetTime get time value from config
func GetTime(key string) time.Time { return v.GetTime(key) }


